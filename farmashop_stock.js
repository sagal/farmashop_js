import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";

const clientId = Deno.env.get('CLIENT_ID')
const clientIntegrationId = Deno.env.get('CLIENT_INTEGRATION_ID')
const clientEcommerce = JSON.parse(Deno.env.get('CLIENT_ECOMMERCE'))

const ftpHost = Deno.env.get('FTP_HOST')
const user = Deno.env.get('FTP_USER')
const pass = Deno.env.get('FTP_PASS')


const init = async () => {

    const decoder = new TextDecoder();

    let client = new FTPClient(ftpHost, {
        user: user,
        pass: pass,
        mode: "passive",
        port: 21,
    });


    await client.connect();
    let stock_data = await downloadFile(client, "./stocks/stock_ecommerce_meli.csv")
    let stock_data_lines = decoder.decode(stock_data).split("\n")
    stock_data_lines = stock_data_lines.splice(1)
    let payloads = []
    for (let line of stock_data_lines) {
        try {
            if (line) {
                let fields = line.split(';')
                let sku = fields[0].trim()
                let stock = Number(fields[1].trim()) 
                stock = stock > 0 ? stock : 0
                let payload = {
                    sku: sku,
                    client_id: clientId,
                    integration_id: clientIntegrationId,
                    options: {
                        merge: false,
                    },
                    ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
                        return {
                            ecommerce_id: ecommerce_id,
                            variants: [],
                            properties: [
                                { "stock": stock }
                            ]
                        }
                    })
                }
                payloads.push(payload)
            }
        } catch (e) {
            console.log(`Could not send article: ${e.message}`)
        }
    }
    await sagalDispatchBatch({
        products: payloads,
        batch_size: 100
    })
    await client.close();

}

async function downloadFile(client, file) {
    try {
        let data = await client.download(file);
        return data
    } catch (e) {
        return null
    }
}


await init();
